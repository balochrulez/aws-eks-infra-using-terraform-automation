terraform {
  backend "s3" {
    bucket         = "ts4u-prod"
    region         = "us-east-1"  # Replace with your AWS region
    key            = "terraform.tfstate"
       
  }
}
